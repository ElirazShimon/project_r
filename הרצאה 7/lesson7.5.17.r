spam<-read.csv('spam.csv', stringsAsFactors = FALSE) #קורא את הקובץ ספאם
spam#מריץ את הקובץ בפועל
#strings as factor = false זה קורא את הקובץ כסטרינג ולא כפקטור

spam$type <- as.factor(spam$type) #קורא את העמודה של type כפקטור

str(spam)#מאפשר לי לחקור את הערכים ומספק לי מיידע עליהם

#text minig package
install.packages('tm')
library(tm)

spam_corpus <- Corpus(VectorSource(spam$text)) #מייצרים מבנה נתונים של קורפוס
#קורפוס מבנה נתונים שמכיל דוקיומנטס ולכל דוקיומנטס אפשר להוסיף נתונים נוספים כגון תאריך כתיב, כותב וכן הלאה.
spam_corpus[[1]][[1]]#כאשר האחד הראשון הוא הדוקיומנט והאחד השני הוא הערך הראשון
spam_corpus[[1]][[2]]#the second colum is a metadata


clean_corpus <- tm_map(clean_corpus,removePunctuation)# take the tm document and remove the puncutation(הורדת סימני הפיסוק)

clean_corpus <- tm_map(clean_corpus,removeNumbers)#numbers remove from document

clean_corpus <- tm_map(clean_corpus,content_transformer(tolower))#change to lowercase all words

clean_corpus <- tm_map(clean_corpus,removeWords,stopwords())#remove the stop words 

clean_corpus <- tm_map(clean_corpus,stripWhitespace)#remove backspace

clean_corpus [[1]][[1]]

dtm <- DocumentTermMatrix(clean_corpus)#create matrix with corpus input

dtm


###############################################LESSON 7/05/2017 #########################################


dim(dtm)


frequent_dtm <- DocumentTermMatrix(clean_corpus,list(dictionary=findFreqTerms(dtm,10)))# find words that show atlist 10 times in all 500 documents

inspect(frequent_dtm[1:500,1:20])#show word that most showed in documents


install.packages('wordcloud')
library(wordcloud)
pal <- brewer.pal(8,'Dark2')


wordcloud(clean_corpus[spam$type =='ham'],min.freq = 5, 
          random.order = FALSE, colors = pal) ##   show words that no! spam and atylist 5 time showing. min.freq its words frequancy 


wordcloud(clean_corpus[spam$type =='spam'],min.freq = 5, 
          random.order = TRUE, colors = pal) ##   show words that spam and atylist 5 time showing.

#spliting to training and testing data sets
vec<- runif(500)# r unif התפלגות אחידה
split <- vec > 0.3 #70% מהרשומות טריינינג סט
split


#spliting the raw data

train_raw <- spam[split,]# split the raw, if want to split the colum put , before split
dim(train_raw)# dimantion of train_raw vector
test_raw <- spam[!split,]
dim(test_raw)




#spliting the corpus data

train_corpus <- spam[split,]# split the raw, if want to split the colum put , before split
dim(train_corpus)# dimantion of train_raw vector
test_corpus <- spam[!split,]
dim(test_corpus)

#spliting document term matrix
train_dtm <- frequent_dtm[split,]
test_dtm <- frequent_dtm[!split,]

#convert the frequency to yes/no

conv_yesno <- function(x){
  x <- ifelse(x>0,1,0)
  x <- factor(x, level = c(1,0), labels = c('yes', 'no'))
  
}

train <- apply(train_dtm, MARGIN = 1:2, conv_yesno)#margin 1:2 that run to all objects in matrix, apply its function like for loops
test <- apply(test_dtm, MARGIN = 1:2, conv_yesno)

train[1:10, 1:5]# run 1 to 10 un raws and 1-5 colums

test[1:10, 1:5]# run 1 to 10 un raws and 1-5 colums


#convert the matrix into data frames
df_train = as.data.frame(train) 
df_test = as.data.frame(test)

#add type ham or spam
df_train$type <- train_raw$type 
df_test$type <- test_raw$type 

dim(df_train)

df_train[1:10,59:60]

df_test[1:10,59:60]


#applying naiive base

install.packages('e1071')
library(e1071)

model <- naiveBayes(df_train[,-60],df_train$type)# -100 that remove the columb number 60 that before we create

model

#confusion matrix to test set

prediction <- predict(model, df_test[,-60])
prediction


install.packages('SDMTools')
library(SDMTools)


conv_10 <- function(x){
  x<- ifelse(x=='spam',1,0)
  }

pred01 <- sapply(prediction, conv_10)
actual01 <- sapply(df_test$type, conv_10)


confusion.matrix(actual01, pred01)

#confusion matrix to training set

prediction_training <- predict(model, df_train[,-60])
prediction_training



conv_20 <- function(x){
  x<- ifelse(x=='spam',1,0)
}

pred02 <- sapply(prediction_training, conv_20)
actual02 <- sapply(df_train$type, conv_20)


confusion.matrix(actual02, pred02)


