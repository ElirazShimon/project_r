
#אלגוריתם רגרסיה לוגיסטית

salesData<-read.csv('WA_Fn-UseC_-Sales-Win-Loss.csv') 
#spliting to training set and test set
split <- runif(nrow(salesData))>0.3
split
#ביצוע החלוקה
salesTrain = salesData[split,]
salesTest = salesData[!split,]

#חבילות של האלגוריתם
install.packages('rpart')
library(rpart)

install.packages('rpart.plot')
library(rpart.plot)

#שימוש במודל של רגרסיה לוגיסטית
logmodel <- glm(Opportunity.Result ~ Supplies.Subgroup + Supplies.Group + Region + Route.To.Market + Elapsed.Days.In.Sales.Stage  + Sales.Stage.Change.Count +Total.Days.Identified.Through.Closing +
                   Total.Days.Identified.Through.Qualified + Opportunity.Amount.USD + Client.Size.By.Revenue + Client.Size.By.Employee.Count +
                   Revenue.From.Client.Past.Two.Years+ Competitor.Type + Ratio.Days.Identified.To.Total.Days + Ratio.Days.Validated.To.Total.Days 
                 +Ratio.Days.Qualified.To.Total.Days + Deal.Size.Category, 
                 data = salesTrain,
                 family="binomial")#יש לי שתי תוצאות
logmodel
#נותן אינפורמציה סטיסטית
summary(logmodel)

predicted <- predict(logmodel, salesData[,-7], type ="response")
predicted[0:10]

predicted01 <-ifelse(predicted>0.5,1,0)
predicted01[1:10]

conv_10 <- function(x){
  x <- ifelse(x == 'Won',1,0)
}
actual01 <- sapply(salesData$Opportunity.Result, conv_10)
actual01[1:10]

install.packages('SDMTools')
library(SDMTools)

confusion.matrix(actual01,predicted01)




#הרצאה 10

install.packages('gains')
library(gains)


gains <- gains(actual = actual01, predicted = predicted, groups = 20)

print(gains)

is.list(gains)


matplot(gains[[1]],cbind(gains[[6]]*100, gains[[1]]), type ="l")

#להציג עוד גרף
matplot(gains[[1]],cbind(gains[[6]]*100, gains[[1]],gains[[8]], gains[[1]]), type ="l")


